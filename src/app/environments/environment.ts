// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const API_HOST = `http://101.53.158.10:3000`;
// const API_HOST = `http://192.168.0.134:4000`;
// const API_HOST = `//${location.hostname}:3000`;
const ANALYSIS = {
  inputImagesBaseURL: API_HOST
};

export const environment = {
  production: false,
  BASE_API_URL: `${API_HOST}/api`,
  ANALYSIS,
  API_HOST,
  IMAGE_URL: `http://101.53.158.10:3000`
  // IMAGE_URL: `http://192.168.0.134:4000`
  // IMAGE_URL: `//${location.hostname}:3000`
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
