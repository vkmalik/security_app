import { Component, Input, OnInit } from '@angular/core';

import { NbMenuService, NbSidebarService } from '@nebular/theme';
import { UserService } from '../../../@core/data/users.service';
import { AnalyticsService } from '../../../@core/utils/analytics.service';
import { TokenService } from '../../../pages/services/token/token.service';
import { ProviderService } from '../../../pages/services/provider/provider.service';
import { Router } from '@angular/router';
import { SettingsService } from '../../../pages/services/settings/settings.service';


@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {

  @Input() position = 'normal';

  user: any;

  userMenu = [{ title: 'Profile', link: '../../pages/company/profile' }, { title: 'Log out', link: '../../auth/login' }];

  constructor(private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    private userService: UserService,
    private analyticsService: AnalyticsService,
    private TokenServ: TokenService,
    private ProviderServ: ProviderService,
    private router: Router,
    private SettingsSerc: SettingsService) {
  }

  ngOnInit() {
    this.userService.getUsers()
    // .subscribe((users: any) => this.user = users.nick);
    // .subscribe(res => {
    //   console.log('line 30....')
    //   console.log(res);
    // },
    //   error => {
    //     console.log(error)
    //   })

    this.getAllProfileDetails();
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');

    return false;
  }

  toggleSettings(): boolean {
    this.sidebarService.toggle(false, 'settings-sidebar');

    return false;
  }

  goToHome() {
    this.menuService.navigateHome();
  }

  startSearch() {
    this.analyticsService.trackEvent('startSearch');
  }

  async getAllProfileDetails() {
    const getProviderId = this.TokenServ.getDecodedToken();
    const { providerId } = getProviderId;
    if (providerId) {
      this.getSelectedProviderDetails(providerId)
    }
  }

  provider: any = {};
  profilepicture;
  profilePic;
  error;
  async getSelectedProviderDetails(id) {
    const result: any = await this.ProviderServ.getSelectedProvide(id);
    if (!result.profilePic) {
      this.profilepicture = './assets/user-avatar.png'
    } else {
      this.profilePic = `${this.SettingsSerc.IMAGE_URL}${result.profilePic}`;
    }
  }
}
