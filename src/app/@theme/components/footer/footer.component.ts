import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by">© 2019 All Rights Reserved. Designed & Developed By <b><a href="https://veniteck.com" target="_blank">Veniteck Solutions</a></b> </span>`,
})
export class FooterComponent {
}
