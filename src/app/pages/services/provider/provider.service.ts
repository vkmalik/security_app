import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { SettingsService } from '../settings/settings.service';

@Injectable({
  providedIn: 'root'
})
export class ProviderService {

  constructor(
    private settingsServ: SettingsService,
    private http: HttpClient
  ) { }

  // Create the owner API

  userRegister(data){
    return new Promise((resolve, reject) => {
      this.http.post(`${this.settingsServ.BASE_API_URL}/provider/register`, data)
        .subscribe(res => {
          resolve(res)
        },
          error => {
            reject(error)
          })
    })
  }

  // Create New Provider API

  createProvide(data) {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.settingsServ.BASE_API_URL}/provider`, data)
        .subscribe(res => {
          resolve(res)
        },
          error => {
            reject(error)
          })
    })
  }



  // Get All Provider List

  getAllProvide() {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.settingsServ.BASE_API_URL}/provider/`)
        .subscribe(res => {
          resolve(res)
        },
          error => {
            reject(error)
          })
    })
  }


  // Get Selected Provider Detail

  getSelectedProvide(id) {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.settingsServ.BASE_API_URL}/provider/${id}`)
        .subscribe(res => {
          resolve(res)
        },
          error => {
            reject(error)
          })
    })
  }


  // Update Selected Provider Details

  updateSelectedProvide(id, data) {
    return new Promise((resolve, reject) => {
      this.http.put(`${this.settingsServ.BASE_API_URL}/provider/${id}`, data)
        .subscribe(res => {
          resolve(res)
        },
          error => {
            reject(error)
          })
    })
  }

}
