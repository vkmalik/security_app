import { Injectable } from '@angular/core';
import { SettingsService } from '../settings/settings.service';
import { HttpClient } from '@angular/common/http';
import { resolve } from 'dns';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(
    private settingsServ: SettingsService,
    private http: HttpClient
  ) { }


  // Create New Employee API

  createEmployee(data) {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.settingsServ.BASE_API_URL}/employee`, data)
        .subscribe(res => {
          resolve(res)
        },
          error => {
            reject(error)
          })
    })
  }

  // Get All Employee List

  getAllEmployees() {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.settingsServ.BASE_API_URL}/employee`)
        .subscribe(res => {
          resolve(res)
        },
          error => {
            reject(error)
          })
    })
  }

  getAllEmployee(id) {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.settingsServ.BASE_API_URL}/employee/company/${id}`)
        .subscribe(res => {
          resolve(res)
        },
          error => {
            reject(error)
          })
    })
  }


  // Get Selected Employee Detail

  getSelectedEmployee(id) {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.settingsServ.BASE_API_URL}/employee/${id}`)
        .subscribe(res => {
          resolve(res)
        },
          error => {
            reject(error)
          })
    })
  }


  // Update Selected Employee Details

  updateSelectedEmployee(id, data) {
    return new Promise((resolve, reject) => {
      this.http.put(`${this.settingsServ.BASE_API_URL}/employee/${id}`, data)
        .subscribe(res => {
          resolve(res)
        },
          error => {
            reject(error)
          })
    })
  }
}
