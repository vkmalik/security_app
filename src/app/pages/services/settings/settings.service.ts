import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class SettingsService {
  BASE_URL = environment.API_HOST;
  BASE_API_URL = environment.BASE_API_URL;
  ANALYSIS = environment.ANALYSIS;
  IMAGE_URL = environment.IMAGE_URL;
  constructor() { }
}
