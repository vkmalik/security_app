import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class TokenService {

  private tokenKey = 'token';
  constructor() { }
  
  userSubject = new BehaviorSubject(false);

  get token() {
    return localStorage.getItem(this.tokenKey);
  }

  getToken() {
    return (localStorage.getItem(this.tokenKey) || '');
  }

  set token(token) {
    localStorage.setItem(this.tokenKey, token);
  }

  get isTokenExpired() {
    const isExpired = !this.token;
    return isExpired;
  }

  getDecodedToken() {
    const token = this.getToken();
    const partialToken = token.split('.')[1];
    if (!partialToken) {
      return null;
    }
    const decodedData = JSON.parse(window.atob(partialToken));
    return decodedData;
  }

  removeToken() {
    localStorage.removeItem(this.tokenKey);
  }

  getUser() {
    return localStorage.getItem('token');
  }
}
