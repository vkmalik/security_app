import { Injectable } from '@angular/core';
import { SettingsService } from '../settings/settings.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  constructor(
    private settingsServ: SettingsService,
    private http: HttpClient
  ) { }



  // Create New Employee API

  createLocation(data) {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.settingsServ.BASE_API_URL}/location`, data)
        .subscribe(res => {
          resolve(res)
        },
          error => {
            reject(error)
          })
    })
  }


  // Get All Employee List

  getAllLocation(id) {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.settingsServ.BASE_API_URL}/location/site/${id}`)
        .subscribe(res => {
          resolve(res)
        },
          error => {
            reject(error)
          })
    })
  }


  // Get Selected Employee Detail

  getSelectedEmployee(id) {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.settingsServ.BASE_API_URL}/location/${id}`)
        .subscribe(res => {
          resolve(res)
        },
          error => {
            reject(error)
          })
    })
  }


  // Update Selected Employee Details

  updateSelectedEmployee(id, data) {
    return new Promise((resolve, reject) => {
      this.http.put(`${this.settingsServ.BASE_API_URL}/location/${id}`, data)
        .subscribe(res => {
          resolve(res)
        },
          error => {
            reject(error)
          })
    })
  }
}
