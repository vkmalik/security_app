// import { Injectable } from '@angular/core';
// import {
//   HttpRequest,
//   HttpHandler,
//   HttpEvent,
//   HttpInterceptor, HttpErrorResponse
// } from '@angular/common/http';
// import { TokenService } from '../token/token.service';
// import { Observable, of } from 'rxjs';
// import { Router } from '@angular/router';
// import { catchError } from 'rxjs/operators/catchError';
// import { UserAuthService } from '../user-auth/user-auth.service';

// const whiteListedHosts = [
//   '//localhost:4200'
// ];

// @Injectable({
//   providedIn: 'root'
// })
// export class InterceptorService implements HttpInterceptor {

//   constructor(
//     private tokenServ: TokenService,
//     private userServ: UserAuthService,
//     private router: Router
//   ) { }
//   intercept(
//     req: HttpRequest<any>,
//     next: HttpHandler): Observable<HttpEvent<any>> {

//     const token = this.tokenServ.token;
//     const isLocalhost = req.url.includes('localhost');
//     const isSameOrigin = req.url.includes(location.hostname);
//     const isWhitelisted = whiteListedHosts.some(url => req.url.includes(url));
//     let transformedReq = req;
//     const isAllowedToMakeRequest = (isLocalhost || isSameOrigin || isWhitelisted);

//     if (token && isAllowedToMakeRequest) {
//       transformedReq = req.clone({
//         setHeaders: {
//           'Authorization': `Bearer ${token}`
//         }
//       });
//     }

//     return next.handle(transformedReq)
//       .pipe(catchError((error, caught) => {
//         this.handleAuthError(error);
//         return of(error);
//       }) as any);
//   }

//   private handleAuthError(err: HttpErrorResponse): Observable<any> {
//     if (err.status === 401 || err.status === 403) {
//       this.userServ.logout();
//       return of(err.message);
//     }
//     throw err;
//   }
// }


