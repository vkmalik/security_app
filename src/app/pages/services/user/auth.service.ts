import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { SettingsService } from '../settings/settings.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private settingsServ: SettingsService,
    private http: HttpClient
  ) { }

  // User Login API

  userLogin(data) {
    // console.log(data)
    return new Promise((resolve, reject) => {
      this.http.post(`${this.settingsServ.BASE_API_URL}/login/`, data)
        .subscribe(res => {
          resolve(res)
        },
          error => {
            reject(error)
          })
    })
  }


  // get logined employees based on siteId
  getLoginedEmployees(id) {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.settingsServ.BASE_API_URL}/login/${id}`)
        .subscribe(res => {
          resolve(res)
        }, error => {
          reject(error)
        })
    })
  }


  // get all user emailID
  getUserEmail(data) {
    let payload = JSON.stringify(data);
    return new Promise((resolve, reject) => {
      this.http.get(`${this.settingsServ.BASE_API_URL}/login/checkemail/${payload}`)
        .subscribe(res => {
          resolve(res)
        }, error => {
          reject(error)
        })
    })
  }

  // user logout emailID
  userLogoutEmail(data) {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.settingsServ.BASE_API_URL}/login/logout`, data)
        .subscribe(res => {
          resolve(res)
        }, error => {
          reject(error)
        })
    })
  }

  // user logout emailID
  updateLoginStatus(data) {
    return new Promise((resolve, reject) => {
      this.http.put(`${this.settingsServ.BASE_API_URL}/login/update`, data)
        .subscribe(res => {
          resolve(res)
        }, error => {
          reject(error)
        })
    })
  }

  // user 
  userLogInEmail(data) {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.settingsServ.BASE_API_URL}/login/loggedIn/${data}`)
        .subscribe(res => {
          resolve(res)
        }, error => {
          reject(error)
        })
    })
  }

// all users
  getAllUsers() {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.settingsServ.BASE_API_URL}/login/users`)
        .subscribe(res => {
          resolve(res)
        }, error => {
          reject(error)
        })
    })
  }
}
