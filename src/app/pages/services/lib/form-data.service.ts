import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FormDataService {

  constructor() { }

  changeFormdataServ(fileName, url, data = {}) {
    const fd = new FormData();
    fd.append(fileName, url);
    const getObjectKeys = Object.keys(data);
    getObjectKeys.forEach(ele => {
      if (ele !== 'profilePic') {
        fd.append(ele, data[ele])
      }
    })
    return fd;
  }

  multieFormdataServ(urls, data = {}) {
    const fd = new FormData();
    if (urls) {
      urls.forEach((ele: any) => {
        fd.append(ele['fileName'], ele['file'])
      })
    }
    const getObjectKeys = Object.keys(data);
    getObjectKeys.forEach(ele => {
      if (!(ele === 'profilePic' || ele === 'governmentIdCardFrontImg'
        || ele === 'governmentIdCardBackImg' || ele === 'companies')) {
        fd.append(ele, data[ele])
      }

      if (ele === "companies") {
        fd.append(ele, JSON.stringify(data[ele]))
      }
    })
    return fd;
  }
}
