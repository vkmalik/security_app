import { Injectable } from '@angular/core';
import { SettingsService } from '../settings/settings.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SiteService {

  constructor(
    private settingsServ: SettingsService,
    private http: HttpClient
  ) { }


  // Create New Site API

  createSite(data) {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.settingsServ.BASE_API_URL}/site`, data)
        .subscribe(res => {
          resolve(res)
        },
          error => {
            reject(error)
          })
    })
  }



  // Get All Site List

  getAllSite(id) {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.settingsServ.BASE_API_URL}/site/company/${id}`)
        .subscribe(res => {
          resolve(res)
        },
          error => {
            reject(error)
          })
    })
  }


  // Get Selected Site Detail

  getSelectedSite(id) {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.settingsServ.BASE_API_URL}/site/${id}`)
        .subscribe(res => {
          resolve(res)
        },
          error => {
            reject(error)
          })
    })
  }


  // Update Selected Site Details

  updateSelectedsite(id, data) {
    return new Promise((resolve, reject) => {
      this.http.put(`${this.settingsServ.BASE_API_URL}/site/${id}`, data)
        .subscribe(res => {
          resolve(res)
        },
          error => {
            reject(error)
          })
    })
  }

}
