import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { SettingsService } from '../settings/settings.service';

@Injectable({
  providedIn: 'root'
})
export class ScanService {

  constructor(
    private settingsServ: SettingsService,
    private http: HttpClient
  ) { }


  // Get All ScanList List

  getScanRecord() {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.settingsServ.BASE_API_URL}/scan/allScans`)
        .subscribe(res => {
          resolve(res)
        },
          error => {
            reject(error)
          })
    })
  }

  getAllScanRecords(data) {
    return new Promise((resolve, reject) => {
      this.http.put(`${this.settingsServ.BASE_API_URL}/scan`, data)
        .subscribe(res => {
          resolve(res)
        },
          error => {
            reject(error)
          })
    })
  }
}
