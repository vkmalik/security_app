import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ModalComponent } from 'angular-custom-modal';

import { AlertDialogService } from '../../../alert-dialog/alert-dialog.service';
import { ConfirmationDialogService } from '../../../confirmation-dialog/confirmation-dialog.service';
import { CompanyService } from '../../services/company/company.service';
import { CurrencyService } from '../../services/currency/currency.service';
import { EmployeeService } from '../../services/employee/employee.service';
import { FormDataService } from '../../services/lib/form-data.service';
import { ProviderService } from '../../services/provider/provider.service';
import { SettingsService } from '../../services/settings/settings.service';
import { SiteService } from '../../services/site/site.service';
import { TokenService } from '../../services/token/token.service';
import { AuthService } from '../../services/user/auth.service';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.scss']
})
export class CreateEmployeeComponent implements OnInit {
  user: any = { siteId: '' };
  providerId;
  editShow = false;
  siteIdList: any = [];
  userSelectedFile = [];
  companyId = '';
  userRole;
  id;
  date;
  providerList: any;
  selected = false;

  company: FormGroup;
  dropdownList = [];
  dropdownSettings = {};
  selectedItems = [];
  regExpr = /[₹,]/g;

  @ViewChild('assignCompany') assignCompany: ModalComponent;

  roles = [{ 'role': 'Manager' }, { 'role': 'Employee' }];
  maritialStatus = [
    { 'value': 'Married', 'name': 'Married' },
    { 'value': 'unMarried', 'name': 'Un-Married' },
    { 'value': 'Single', 'name': 'Single' },
  ];
  qualifications = [
    { 'value': 'Primary education', 'name': 'Primary education' },
    { 'value': 'Secondary education', 'name': 'Secondary education or high school' },
    { 'value': `Bachelor's degree`, 'name': `Bachelor's degree` },
    { 'value': `Master's degree`, 'name': `Master's degree` },
    { 'value': 'Doctorate or higher', 'name': 'Doctorate or higher' }
  ];
  idproof = [
    { 'value': 'Aadhar card', 'name': 'Aadhar card' },
    { 'value': 'VoterId', 'name': 'VoterId or high school' },
    { 'value': 'PanCard', 'name': 'PanCard' }
  ];

  constructor(
    private curr: CurrencyService,
    private fb: FormBuilder,
    private CompanyServ: CompanyService,
    private modalService: NgbModal,
    private EmployeeServ: EmployeeService,
    private router: Router,
    private route: ActivatedRoute,
    private SiteServ: SiteService,
    private TokenServ: TokenService,
    private FormDataServ: FormDataService,
    private SettingsSerc: SettingsService,
    private AuthServ: AuthService,
    private ProviderServ: ProviderService,
    private confirmationDialogService: ConfirmationDialogService,
    private AlertDialog: AlertDialogService,
  ) {

    const result = this.TokenServ.getDecodedToken();
    const { companyId, role, providerId } = result;
    this.providerId = providerId;
    this.userRole = role
    this.date = new Date();
    // this.getProviderList();
    this.getCompanyList();

    this.company = this.fb.group({
      items: this.fb.array(
        [this.createItem()]
      )
    })
  }
  createItem() {
    return this.fb.group({
      companyId: '',
      siteIds: ''
    });
  }

  items;
  addItem(): void {
    this.items = this.company.get('items') as FormArray;
    this.items.push(this.createItem());
  }

  RemoveItem(index) {
    this.items.removeAt(index);
  }


  ngOnInit() {
    let getID = this.route.snapshot.paramMap.get("id");
    if (getID === '' || getID === undefined || getID === null) {
      return this.user = {}
    }
    this.companyId = getID;
    if (!getID) {
      const getProviderId = this.TokenServ.getDecodedToken();
      const { companyId } = getProviderId;
      this.companyId = companyId;
    }

    this.getEmployeeDetail(getID);

  }

  onItemSelect(item: any) {
    // console.log(item);
  }
  OnItemDeSelect(item: any) {
    // console.log(item);
  }
  onSelectAll(items: any) {
    // console.log(items);
  }
  onDeSelectAll(items: any) {
    // console.log(items);
  }


  // Get All Provider List
  companyLists: any = [];
  // async getProviderList() {
  //   try {
  //     const getValue: any = await this.ProviderServ.getAllProvide();
  //     getValue.forEach(ele => {
  //       if (ele.role !== 'super_admin') {
  //         this.companyLists.push(ele);
  //       }
  //     });
  //   }
  //   catch (error) {
  //     console.log(error)
  //   }
  // }

  companyArray;
  async getCompanyList() {
    const getProviderId = this.TokenServ.getDecodedToken();
    const { providerId } = getProviderId;
    const result: any = await this.CompanyServ.getAllproviderCompany(providerId);
    this.companyArray = result;
    this.companyArray.forEach(ele => {
      this.companyLists.push(ele);
    })
  }

  siteList;
  async getSiteDetail(id) {
    const result: any = await this.SiteServ.getAllSite(id);
    if (result.length !== 0) {
      this.selected = true;
      this.siteList = result;

      this.dropdownList = result;

      this.dropdownSettings = {
        singleSelection: false,
        idField: 'siteId',
        textField: 'siteName',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 5,
        allowSearchFilter: false
      };
    } else {
      this.selected = false;
      return await this.AlertDialog.confirm('Alert Message', 'No post are added for this company');
    }
  }

  companiessss = [];
  anyData;
  length = false;
  onSubmit() {
    this.companiessss = [];
    this.company.value.items.forEach(ele => {
      this.companyLists.forEach(data => {
        if (ele.companyId === data.companyId) {
          Object.assign(ele, { companyName: data.companyName })
        }
      })
      this.companiessss.push(ele);
    })

    if (this.editShow === true) {
      if (this.companiessss.length !== 0 && this.companiessss[0].companyId !== '') {
        this.length = true;
      }
    }

    if (this.editShow === true) {
      if (this.companyList.length !== 0) {
        if (this.companiessss.length !== 0) {
          this.companiessss.forEach((ele, j) => {
            this.companyList.forEach(async (data, i) => {
              if (data.companyId === ele.companyId) {
                const resultAleart = await this.confirmationDialogService.confirm('Alert Message', data.companyName + ' is already assigned. Do you want to delete the existing one?');
                if (resultAleart) {
                  this.companyList.splice(i, 1);
                  this.companyList.push(ele);
                  if (this.companyList.length === 0) {
                    this.listEmpty = true;
                  } else {
                    this.listEmpty = false;
                  }
                } else {
                  if (this.companiessss.length === 1) {
                    this.companiessss.splice(j, 1);
                    this.addItem();
                    this.RemoveItem(j)
                    this.length = false;
                  } else {
                    this.companiessss.splice(j, 1);
                    this.RemoveItem(j)
                    if (this.companiessss.length === 0) {
                      this.length = false;
                      this.addItem();
                    }
                  }
                }
              } else {

              }
              if (this.companiessss.length !== 0 && this.companiessss[0].companyId !== '' && this.companiessss[0].siteIds.length !== 0) {
                this.length = true;
                this.modalRef.close();
              }
            })
          })
        } else {
          return;
        }
      } else {
        this.companyList = this.companiessss;
        this.listEmpty = false;
        this.length = true;
        this.modalRef.close();
      }
    }

  }

  roleSelect(event) {
    this.roleSelected = false
  }

  // Create New Company
  roleSelected = false;
  DOBSelected = false;
  async createNewEmployee(formData) {
    if (this.editShow === true) {
      if (this.companyList.length !== 0) {
        if (this.companiessss.length !== 0) {
          this.companiessss.forEach(ele => {
            this.companyList.forEach((data, i) => {
              if (data.companyId === ele.companyId) {

              } else {
                this.companiessss.push(data)
              }
            })
          })
        }
      }
    }

    if (this.editShow !== true) {
      if (formData.value.role === '') {
        this.roleSelected = true
      } else {
        this.roleSelected = false
      }

      if (formData.value.presentAddress === undefined ||
        formData.value.permanentAddress === undefined ||
        formData.value.role === '' ||
        formData.value.employeeName === undefined ||
        formData.value.password === undefined ||
        formData.value.email === undefined ||
        formData.value.siteId === '' ||
        formData.value.mobile === undefined) {
        return await this.AlertDialog.confirm('Alert Message', 'Please Fill the Mandatory Fields');

      }

      if (this.user.age === undefined) {
        this.DOBSelected = true;
        return await this.AlertDialog.confirm('Alert Message', 'Please enter correct DOB');
      } else {
        this.DOBSelected = false;
      }

    }

    const { value } = formData;
    const result = this.TokenServ.getDecodedToken();
    const { providerId, userId, name } = result;

    Object.assign(value, { providerId, userId, createdBy: name, companies: this.companiessss })
    if (!this.companyId) {
      Object.assign(value, { providerId, userId, createdBy: name, companies: this.companiessss })
    }

    try {
      if (this.editShow === true) {
        if (this.companiessss.length !== 0) {
          delete this.user.companies

          Object.assign(this.user, { companies: this.companiessss })
        } else {

        }
        const fdData = this.FormDataServ.multieFormdataServ(this.userSelectedFile, this.user)
        let result: any = await this.EmployeeServ.updateSelectedEmployee(this.user.employeeId, fdData)
        // return this.router.navigate([`./pages/company/employees/${result.companyId}`])
        return this.router.navigate([`./pages/company/employees`])

      }
      const fdData = this.FormDataServ.multieFormdataServ(this.userSelectedFile, value)
      let result: any = await this.EmployeeServ.createEmployee(fdData);
      // return this.router.navigate([`./pages/company/employees/${this.companyId}`])
      return this.router.navigate([`./pages/company/employees`])

    }
    catch (error) {
      console.log(error)
    }
  }

  // Get Prarticular Company Details
  companyList;
  listEmpty = false;
  employeeName;
  async getEmployeeDetail(id) {
    try {
      const result: any = await this.EmployeeServ.getSelectedEmployee(id)
      if (result.message === "No Record Found this ID") {
        this.editShow = false;
        return
      }

      result.profilePic = `${this.SettingsSerc.IMAGE_URL}${result.profilePic}`
      result.governmentIdCardFrontImg = `${this.SettingsSerc.IMAGE_URL}${result.governmentIdCardFrontImg}`
      result.governmentIdCardBackImg = `${this.SettingsSerc.IMAGE_URL}${result.governmentIdCardBackImg}`;
      this.user = result;
      this.employeeName = '';
      this.employeeName = this.user.employeeName;
      // alert(this.employeeName)

      if (result.previousSalary[0] === '₹') {
        result.previousSalary = result.previousSalary.replace(this.regExpr, "");
      }

      if (result.presentSalary[0] === '₹') {
        result.presentSalary = result.presentSalary.replace(this.regExpr, "");
      }
      this.companyList = result.companies;
      if (this.companyList.length === 0) {
        this.listEmpty = true;
      } else {
        this.listEmpty = false;
      }
      this.user.previousSalary = this.curr.convertToInrFormat(result.previousSalary);
      this.user.presentSalary = this.curr.convertToInrFormat(result.presentSalary);
      this.editShow = true;
      this.id = result.companyId;
      this.siteDetail(result.companyId)
      this.getUsers(this.user.userId);
    }
    catch (error) {
      console.log(error)
    }



  }

  // Get Particular Company Site Detail

  async siteDetail(id) {
    const result: any = await this.SiteServ.getAllSite(id);
    if (result.message === 'No Record Found this ID') {
      return;
    }
    this.siteIdList = result;
  }

  // Select Company Logo Process

  readURL(files, fileName) {
    const [file] = files;
    this.userSelectedFile.push({ fileName, file });
  }

  close() {
    this.router.navigate([`./pages/company/employees`])
  }


  prevSalary;
  changePrevious(event) {
    this.prevSalary = '';
    if (event[0] === '₹') {
      event = event.replace(this.regExpr, "");
    }
    this.prevSalary = this.curr.convertToInrFormat(event);
    this.user.previousSalary = this.prevSalary;
  }


  presSalary;
  changePresent(event) {
    this.presSalary = '';
    if (event[0] === '₹') {
      event = event.replace(this.regExpr, "");
    }
    this.presSalary = this.curr.convertToInrFormat(event);
    this.user.presentSalary = this.presSalary;
  }

  value: any = {}
  async checkEmail(event) {
    try {
      Object.assign(this.value, { 'email': event })
      const result: any = await this.AuthServ.getUserEmail(this.value);
      if (result.length === 0) {
        return;
      } else {
        await this.AlertDialog.confirm('Alert Message', 'This Email already exists. Please enter a different Email.');
      }
    }
    catch (error) {
      console.log(error);
    }
  }
  modalRef: any;
  assignMultipleCompany(longContent) {
    this.modalRef = this.modalService.open(longContent, { size: 'lg' });
  }

  calAge;
  getAge(dateString) {
    var today = new Date();
    var birthDate = new Date(dateString);
    this.calAge = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      this.calAge--;
    }
    this.user.age = this.calAge;
  }

  removeSite(index) {
    this.companyList.splice(index, 1);
    if (this.companyList.length === 0) {
      this.listEmpty = true;
    } else {
      this.listEmpty = false;
    }
  }

  async toggle() {
    this.loginStatus = !this.loginStatus;
    const data: any = {};
    Object.assign(data, { 'email': this.userEmail, 'isLogin': this.loginStatus })
    const result1: any = await this.AuthServ.updateLoginStatus(data)
  }

  loginStatus;
  userEmail;
  async getUsers(id) {
    const result: any = await this.AuthServ.userLogInEmail(id);
    this.loginStatus = result.isLogin;
    this.userEmail = result.email;
  }
}

