import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { LocalDataSource } from 'ng2-smart-table';

import { AlertDialogService } from '../../../alert-dialog/alert-dialog.service';
import { CompanyService } from '../../services/company/company.service';
import { EmployeeService } from '../../services/employee/employee.service';
import { FormDataService } from '../../services/lib/form-data.service';
import { ProviderService } from '../../services/provider/provider.service';
import { SettingsService } from '../../services/settings/settings.service';
import { SiteService } from '../../services/site/site.service';
import { TokenService } from '../../services/token/token.service';

@Component({
  selector: 'employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss']
})
export class EmployeesComponent implements OnInit {
  empId: any;
  showDetails = false;
  editShow = false;
  user: any = { siteId: '' };
  siteIdList: any = [];
  userSelectedFile = [];
  companyId = '';
  userRole;
  source: LocalDataSource; // add a property to the component


  settings = {
    columns: {
      // slNo: {
      //   title: '#',
      //   filter: false
      // },
      employeeName: {
        title: 'Employee Name',
        filter: false
      },
      DOB: {
        title: 'DOB',
        filter: false
      },
      gender: {
        title: 'Gender',
        filter: false
      },
      mobile: {
        title: 'Mobile',
        filter: false
      },
      joineDate: {
        title: 'Joining Date',
        filter: false
      },
      maritalStatus: {
        title: 'Marital Status',
        filter: false
      },
      qualification: {
        title: 'Qualification',
        filter: false
      },
      presentSalary: {
        title: 'Present Salary',
        filter: false,
        // valuePrepareFunction: (value) => { return value === 'presentSalary' ? value : Intl.NumberFormat('en-US', { style: 'currency', currency: 'INR' }).format(value) },
      },
      previousSalary: {
        title: 'Previous Salary',
        filter: false,
        // valuePrepareFunction: (value) => { return value === 'previousSalary' ? value : Intl.NumberFormat('en-US', { style: 'currency', currency: 'INR' }).format(value) },
      },
      createdBy: {
        title: 'Created By',
        filter: false
      }
    },
    // add: {
    //   addButtonContent: '<i class="nb-plus"></i>',
    //   editButtonContent: '<i class="far fa-eye"></i><i class="far fa-edit"></i>',
    //   deleteButtonContent: '<i class="far fa-eye"></i>',
    // },
    // edit: {
    //   editButtonContent: '<i class="far fa-eye"></i><i class="far fa-edit"></i>',
    // },
    // delete: {
    //   deleteButtonContent: '<i class="far fa-eye"></i>',
    // },

    actions: {
      position: 'right',
      edit: false,
      add: false,
      delete: false
    }
  };

  data = [
    // ... list of items        
  ];

  isLoading = true;
  employeeLists: any = []
  CompanyName;
  role;
  id;
  constructor(
    private CompanyServ: CompanyService,
    private EmployeeServ: EmployeeService,
    private router: Router,
    private route: ActivatedRoute,
    private SiteServ: SiteService,
    private TokenServ: TokenService,
    private FormDataServ: FormDataService,
    private SettingsSerc: SettingsService,
    private ProviderServ: ProviderService,
    private AlertDialog: AlertDialogService
  ) {
    this.source = new LocalDataSource(this.data); // create the source
  }

  async getCompanyDetail(id) {
    const result: any = await this.CompanyServ.getSelectedCompany(id);
    this.CompanyName = result.companyName;
  }

  providerId;
  siteId;
  ngOnInit() {
    let getID = this.route.snapshot.paramMap.get("id");
    const getProviderId = this.TokenServ.getDecodedToken();
    const { companyId, role, providerId } = getProviderId;
    this.role = role;
    this.companyId = getID;
    this.providerId = providerId;
    if (!getID) {
      this.companyId = companyId;
    }
    if (getID === '' || getID === undefined || getID === null) {
      this.totalEmployeeDetail(this.providerId);
      this.getEmployeeDetail(this.providerId);
      this.getCompanyDetail(companyId);
      this.getProviderDetails(providerId);
      return
    }
    this.totalEmployeeDetail(this.providerId);
    this.getEmployeeDetail(getID)
    this.getCompanyDetail(getID)
    this.siteDetail(getID)
    this.getProviderDetails(providerId);
  }

  async addNewEmployee() {
    if (parseInt(this.maxEmployee) === parseInt(this.employeeCount)) {
      await this.AlertDialog.confirm('Alert Message', 'Maximum no of employees allowed are created. Please contact your Provider for further assistance.');
      return;
    } else {
      this.router.navigate(['./pages/company/createEmployees'])
    }
    // this.router.navigate(['./pages/company/createEmployees'])

  }

  onUserRowSelect(event) {

    this.empId = '';
    this.empId = event.data.employeeId;
    this.router.navigate(['./pages/company/createEmployees', this.empId])

  }

  async siteDetail(id) {
    const result: any = await this.SiteServ.getAllSite(id);
    if (result.message === 'No Record Found this ID') {
      return;
    }
    this.siteIdList = result;
  }

  back() {
    this.router.navigate(['./pages/company/list'])
  }


  async getEmployeeDetail(id) {
    const result = await this.EmployeeServ.getAllEmployee(id);
    this.employeeLists = result;
    this.source.load(this.employeeLists);
  }

  employeeCount;
  async totalEmployeeDetail(id) {
    const result = await this.EmployeeServ.getAllEmployee(id);
    this.employeeLists = result;
    this.employeeCount = parseInt(this.employeeLists.length);
  }

  maxEmployee;
  async getProviderDetails(id) {
    try {
      const getValue: any = await this.ProviderServ.getSelectedProvide(id);
      this.maxEmployee = getValue.noOfEmployees;
    }
    catch (error) {
      console.log(error)
    }
  }
}
