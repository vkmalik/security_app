import { Component, OnInit } from '@angular/core';

import { TokenService } from '../../../pages/services/token/token.service';
import { ProviderService } from '../../../pages/services/provider/provider.service';
import { Router } from '@angular/router';
import { SettingsService } from '../../services/settings/settings.service';

@Component({
  selector: 'profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  imgUrl = '';
  providerName;
  provider: any = {

  }
  constructor(
    private TokenServ: TokenService,
    private ProviderServ: ProviderService,
    private router: Router,
    private SettingsSerc: SettingsService
  ) { }

  ngOnInit() {
    this.getAllProfileDetails();
  }

  async getAllProfileDetails() {
    const getProviderId = this.TokenServ.getDecodedToken();
    const { providerId } = getProviderId;
    if (providerId) {
      this.getSelectedProviderDetails(providerId)
    }
  }
  profilepicture;
  profilePic;
  async getSelectedProviderDetails(id) {
    const result: any = await this.ProviderServ.getSelectedProvide(id);
    if (!result.profilePic) {
      this.profilepicture = './assets/user-avatar.png'
    } else {
      this.profilePic = `${this.SettingsSerc.IMAGE_URL}${result.profilePic}`;
    }
    this.provider = result;    
  }

}
