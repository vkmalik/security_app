import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'angular-custom-modal';
import { NbPopoverModule, NbCardModule, NbButtonModule, NbStepperModule, NbSelectModule, NbRadioModule, NbCheckboxModule, NbActionsModule } from '@nebular/theme';

import { CompanyRoutingModule } from './company-routing.module';
import { CompanyComponent } from './company.component';
import { CompanyListComponent } from './company-list/company-list.component';
import { CompanyDetailsComponent } from './company-details/company-details.component';
import { SitesComponent } from './sites/sites.component';
import { EmployeesComponent } from './employees/employees.component';
import { ScansComponent } from './scans/scans.component';

import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ProfileComponent } from './profile/profile.component';
import { LocationsComponent } from './locations/locations.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateEmployeeComponent } from './create-employee/create-employee.component';
import { CreateSitesComponent } from './create-sites/create-sites.component';
import { CompanyListComponents } from './provider/company-list/company-list.component';
import { CompanyComponents } from './provider/company/company.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown-angular7';

@NgModule({
  declarations: [CompanyComponent, CompanyListComponent, CompanyComponents, CompanyListComponents, CompanyDetailsComponent, SitesComponent, EmployeesComponent, ScansComponent, ProfileComponent, LocationsComponent, CreateEmployeeComponent, CreateSitesComponent],
  imports: [
    ModalModule,
    CommonModule,
    CompanyRoutingModule,
    Ng2SmartTableModule,
    NbCardModule,
    NbPopoverModule,
    FormsModule,
    NbRadioModule,
    NbStepperModule,
    NbSelectModule,
    NbCheckboxModule,
    NbActionsModule,
    NbButtonModule,
    NgbModule,
    ReactiveFormsModule,
    NgMultiSelectDropDownModule.forRoot()
  ]
})
export class CompanyModule { }
