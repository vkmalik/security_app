import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';

import { LocalDataSource } from 'ng2-smart-table';

import { AlertDialogService } from '../../../alert-dialog/alert-dialog.service';
import { CompanyService } from '../../services/company/company.service';
import { ProviderService } from '../../services/provider/provider.service';
import { ScanService } from '../../services/scan-details/scan.service';
import { SiteService } from '../../services/site/site.service';
import { TokenService } from '../../services/token/token.service';

@Component({
  selector: 'scans',
  templateUrl: './scans.component.html',
  styleUrls: ['./scans.component.scss']
})

export class ScansComponent implements OnInit {
  loginId;
  date;
  CompanyName;
  id;
  difference: any = 240;
  siteList: any = [];
  scanList: any = [];
  formatted;

  source: LocalDataSource; // add a property to the component

  settings = {
    columns: {
      // slNo: {
      //   title: '#',
      //   filter: false
      // },
      employeeName: {
        title: 'Employee Name',
        filter: false
      },
      scannedDate: {
        title: 'Scanned Date',
        filter: false
      },
      scannedTime: {
        title: 'Scanned Time',
        filter: false
      },
      latitude: {
        title: 'Current Latitude',
        filter: false
      },
      longitude: {
        title: 'Current Longitude',
        filter: false
      },
      QRLong: {
        title: 'QR Longitude',
        filter: false
      },
      QRLatit: {
        title: 'QR Latitude',
        filter: false
      },
      differnceMeters: {
        title: 'Difference Meters',
        filter: false
      },
      locationName: {
        title: 'Location Name',
        filter: false
      },
      scannedType: {
        title: 'Scan Type',
        filter: false
      },
      synced: {
        title: 'Synced',
        filter: false
      },
    },
    // add: {
    //   addButtonContent: '<i class="nb-plus"></i>',
    //   editButtonContent: '<i class="far fa-eye"></i><i class="far fa-edit"></i>',
    //   deleteButtonContent: '<i class="far fa-eye"></i>',
    // },
    // edit: {
    //   editButtonContent: '<i class="far fa-eye"></i><i class="far fa-edit"></i>',
    // },
    // delete: {
    //   deleteButtonContent: '<i class="far fa-eye"></i>',
    // },

    actions: {
      position: 'right',
      edit: false,
      add: false,
      delete: false,
    },
    rowClassFunction: (row) => {
      if (row.data.differnceMeters > parseFloat(row.data.locDifference)) {
        return 'aborted';
      }
      return 'solved'

    }
  };

  data = [
    // ... list of items        
  ];
  constructor(
    private TokenServ: TokenService,
    private CompanyServ: CompanyService,
    private SiteServ: SiteService,
    private route: ActivatedRoute,
    private ScanSer: ScanService,
    private router: Router,
    private datePipe: DatePipe,
    private ProviderServ: ProviderService,
    private AlertDialog: AlertDialogService

  ) {
    this.source = new LocalDataSource(this.data); // create the source
    this.date = new Date();
    this.formatted = this.datePipe.transform(this.date, 'yyyy-MM-dd');

    // this.getProviderList();
    this.getCompanyList();
  }

  dataDefault: any = {};
  routeId;
  ngOnInit() {
    let getID = this.route.snapshot.paramMap.get("id");
    this.routeId = getID;
    const getProviderId = this.TokenServ.getDecodedToken();
    const { companyId, siteId, providerId, userId } = getProviderId;
    const result = this.TokenServ.getDecodedToken();
    this.loginId = userId;
    this.getCompanyDetail(providerId);
    if (!siteId && siteId !== '') {
      // this.getSiteDetail(getID);
      this.getCompanyDetail(getID);
      return
    }
    // this.getSiteDetail(companyId);
    this.getCompanyDetail(companyId);
  }

  // Get All Provider List
  companyList: any = [];
  empty = false;
  companyArray;
  async getCompanyList() {
    const getProviderId = this.TokenServ.getDecodedToken();
    const { providerId } = getProviderId;
    const result: any = await this.CompanyServ.getAllproviderCompany(providerId);
    this.companyArray = result;
    this.companyArray.forEach(ele => {
      this.companyList.push(ele);
    })

    if (this.companyList.length === 0) {
      this.empty = true;
    } else {
      this.empty = false;
      if (this.routeId === null) {
        this.providerName(this.companyList[0].companyId);
      } else {
        this.providerName(this.routeId)
      }
    }
  }

  siteNameList = [];
  loginDetailsList: any;
  defaultSiteId;
  show = false;
  async providerName(event) {               // event is companyId
    const result: any = await this.SiteServ.getAllSite(event);
    if (result.length === 0) {
      this.siteNameList = [];
      this.siteNameList.push({ 'siteId': "0", 'siteName': "No Posts Found" })
      this.show = true;
      return await this.AlertDialog.confirm('Alert Message', 'No posts found for this company.');
    } else {
      this.siteNameList = [];
      result.forEach(ele => {
        this.siteNameList.push({ 'siteId': ele.siteId, 'siteName': ele.siteName })
      })

      this.defaultSiteId = this.siteNameList[0].siteId;

      Object.assign(this.dataDefault, { 'siteId': this.defaultSiteId, 'fromDate': this.formatted, 'lastDate': this.formatted });
      this.createQRCode(this.dataDefault);
    }
  }

  // Get Provide ID base Companys
  async getCompanyDetail(id) {
    try {
      const result: any = await this.CompanyServ.getSelectedCompany(id);
      this.CompanyName = result.companyName;
    }
    catch (error) {
      console.log(error)
    }
  }

  // for fetching siteId
  siteIdSelected;
  siteName(event) {
    this.siteIdSelected = event;
  }

  // Fetch Scan data
  value;
  async createQRCode(formData) {

    if (formData.value === undefined) {
      this.value = formData;
    } else {
      this.value = formData.value;
      if (this.siteIdSelected === undefined) {
        Object.assign(this.value, { 'siteId': this.defaultSiteId })
      } else {
        Object.assign(this.value, { 'siteId': this.siteIdSelected })
      }
    }
    try {
      const result = await this.ScanSer.getAllScanRecords(this.value);
      this.scanList = result;
      this.show = false;
      this.source.load(this.scanList);
    }
    catch (error) {
      console.log(error)
    }

  }

  close() {
    this.router.navigate([`./pages/company/list`])
  }

  back() {
    this.router.navigate(['./pages/company/list'])
  }

  csv: any = {};
  csv1: any = {};
  finalCsv: any = [];
  async generateCsv() {
    const result = await this.ScanSer.getAllScanRecords(this.value);
    this.csv = result;
    if (this.csv.length === 0) {
      return await this.AlertDialog.confirm('Alert Message', 'No result found for the given date interval.');
    } else {
      this.csv.forEach(ele => {
        delete ele.beconeId;
        delete ele.dateTime;
        delete ele._id;
        delete ele.companyId;
        delete ele.siteId;
        delete ele.providerId;
        delete ele.locationId;
        delete ele.employeeId;
        delete ele.scanId;
        delete ele.createdAt;
        delete ele.updatedAt;
        delete ele.__v;
        this.csv1 = {};
        Object.assign(this.csv1, {
          'Scan Date': ele.scannedDate,
          'Scan Time': ele.scannedTime,
          'Site Name': ele.siteName,
          'Location': ele.locationName,
          'Employee Name': ele.employeeName,
          'Latitude': ele.latitude,
          'Longitude': ele.longitude,
          'Difference Meters': ele.differnceMeters,
          'Location Variance': ele.locDifference,
          'Scan Type': ele.scannedType
        })
        this.finalCsv.push(this.csv1)
        console.log(this.finalCsv)
      })
      this.JSONToCSVConvertor(this.finalCsv, "Scan Report", true);
    }
  }

  async  JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

    var CSV = '';
    //Set Report title in first row or line

    CSV += ReportTitle + '\r\n\n';

    //This condition will generate the Label/Header
    if (ShowLabel) {
      var row = "";

      //This loop will extract the label from 1st index of on array
      for (var index in arrData[0]) {
        //Now convert each value to string and comma-seprated
        row += index + ',';
      }

      row = row.slice(0, -1);

      //append Label row with line break
      CSV += row + '\r\n';
    }

    //1st loop is to extract each row
    for (var i = 0; i < arrData.length; i++) {
      var row = "";

      //2nd loop will extract each column and convert it in string comma-seprated
      for (var index in arrData[i]) {
        row += '"' + arrData[i][index] + '",';
      }

      row.slice(0, row.length - 1);

      //add a line break after each row
      CSV += row + '\r\n';
    }

    if (CSV == '') {
      return await this.AlertDialog.confirm('Alert Message', 'Invalid data.');
    }

    //Generate a file name
    var fileName = "MyReport_";
    //this will remove the blank-spaces from the title and replace it with an underscore
    fileName += ReportTitle.replace(/ /g, "_");

    //Initialize file format you want csv or xls
    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

    // Now the little tricky part.
    // you can use either>> window.open(uri);
    // but this will not work in some browsers
    // or you will not get the correct file extension    

    //this trick will generate a temp <a /> tag
    var link = document.createElement("a");
    link.href = uri;

    //set the visibility hidden so it will not effect on your web-layout
    // link.style = "visibility:hidden";
    link.download = fileName + ".csv";

    //this part will append the anchor tag and remove it after automatic click
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }

}


