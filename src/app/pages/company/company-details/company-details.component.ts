import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';

import { AlertDialogService } from '../../../alert-dialog/alert-dialog.service';
import { CompanyService } from '../../services/company/company.service';
import { EmployeeService } from '../../services/employee/employee.service';
import { FormDataService } from '../../services/lib/form-data.service';
import { ProviderService } from '../../services/provider/provider.service';
import { SettingsService } from '../../services/settings/settings.service';
import { TokenService } from '../../services/token/token.service';
import { AuthService } from '../../services/user/auth.service';

@Component({
  selector: 'company-details',
  templateUrl: './company-details.component.html',
  styleUrls: ['./company-details.component.scss']
})
export class CompanyDetailsComponent implements OnInit {
  companyLists: any = [];
  date;
  user: any = {};
  edit = false;
  userSelectedFile;
  constructor(
    private CompanyServ: CompanyService,
    private router: Router,
    private route: ActivatedRoute,
    private TokenServ: TokenService,
    private FormDataServ: FormDataService,
    private SettingsSerc: SettingsService,
    private datePipe: DatePipe,
    private AuthServ: AuthService,
    private EmployeeServ: EmployeeService,
    private AlertDialog: AlertDialogService

  ) {
    this.date = new Date();
    this.getAllCompanyDetails()
  }

  ngOnInit() {
    let getID = this.route.snapshot.paramMap.get("id");
    if (getID === '' || getID === undefined || getID === null) {
      const getProviderId = this.TokenServ.getDecodedToken();
      const { companyId } = getProviderId;
      // this.getCompanyDetail(companyId)
      return;
    }
    this.getCompanyDetail(getID)

  }


  // Create New Company

  async createNewCompany(formData) {
    if (this.edit !== true) {
      this.companyLists.forEach(async (ele) => {
        if (ele.companyName === formData.value.companyName) {
          await this.AlertDialog.confirm('Alert Message', 'Company name already exists. Please enter a different name.');
          return;
        }
      });

      if (formData.value.companyAddress === undefined || formData.value.companyName === undefined || formData.value.password === undefined || formData.value.email === undefined) {
        await this.AlertDialog.confirm('Alert Message', 'Please Fill the Mandatory Fields');
        return;
      }
    }

    const { value } = formData;
    const getProviderId = this.TokenServ.getDecodedToken();
    const { providerId, userId } = getProviderId;
    if (!providerId) {
      this.TokenServ.removeToken();
      return this.router.navigate(['/'])
    }
    Object.assign(value, { providerId, userId });
    try {
      if (this.edit === true) {
        const fdData = this.FormDataServ.changeFormdataServ('companyLogo', this.userSelectedFile, this.user)
        await this.CompanyServ.updateSelectedCompany(this.user.companyId, fdData)
        return this.router.navigate(['./pages/company/list'])
      }
      const fdData = this.FormDataServ.changeFormdataServ('companyLogo', this.userSelectedFile, value)
      let result = await this.CompanyServ.createCompany(fdData)
      if (result) {
        return this.router.navigate(['./pages/company/list'])
      }
    }
    catch (error) {
      console.log(error)
    }
  }

  // Get Prarticular Company Details
  userCompanyId;
  async getCompanyDetail(id) {
    try {
      const result: any = await this.CompanyServ.getSelectedCompany(id);
      result.profilePic = `${this.SettingsSerc.IMAGE_URL}${result.profilePic}`
      this.user = result;
      this.userCompanyId = '';
      this.userCompanyId = this.user.companyId;
      this.edit = true;
      this.getEmployeeDetail()
    }
    catch (error) {
      console.log(error)
    }
  }

  comp: any;
  totalCount;
  presentCount;
  assigned: any = [];
  async getEmployeeDetail() {

    this.totalCount = 0;
    const results = await this.EmployeeServ.getAllEmployees();
    this.comp = results;
    this.comp.forEach(ele1 => {          // for loop for employees
      ele1.companies.forEach(data => {          // for loop for companiesAssigned
        if (data.companyId === this.userCompanyId) {
          this.totalCount++;
        }
      })
    })
    Object.assign(this.user, { 'noOfEmployees': this.totalCount })
  }

  // Select Company Logo Process

  readURL(files) {
    const [file] = files;
    this.userSelectedFile = file;
  }

  close() {
    this.router.navigate(['./pages/company/list']);
  }

  // async checkEmail(event) {
  //   try {
  //     const result: any = await this.AuthServ.getUserEmail(event);
  //     if (!result) {

  //     } else {
  //       await this.AlertDialog.confirm('Alert Message', 'This Email already exists');
  //     }
  //   }
  //   catch (error) {
  //     console.log(error);
  //   }
  // }

  async getAllCompanyDetails() {
    const getProviderId = this.TokenServ.getDecodedToken();
    const { providerId } = getProviderId;

    if (providerId) {
      const result: any = await this.CompanyServ.getAllproviderCompany(providerId);
      this.companyLists = result;
    }
  }

  getSiteRecords(id) {
    this.router.navigate(['./pages/company/post', id])
  }

  getScanRecords(id) {
    this.router.navigate(['./pages/company/scans', id])
  }

  getEmployeeRecords(id) {
    this.router.navigate(['./pages/company/employees', id])
  }

  value: any = {}
  async checkEmail(event) {
    try {
      Object.assign(this.value, { 'email': event })
      const result: any = await this.AuthServ.getUserEmail(this.value);
      if (result.length === 0) {
        return;
      } else {
        await this.AlertDialog.confirm('Alert Message', 'This Email already exists');
      }
    }
    catch (error) {
      console.log(error);
    }
  }
}
