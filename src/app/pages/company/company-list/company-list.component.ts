import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { LocalDataSource } from 'ng2-smart-table';

import { EmployeeService } from '../../services/employee/employee.service';
import { CompanyService } from '../../../pages/services/company/company.service';
import { ProviderService } from '../../../pages/services/provider/provider.service';
import { TokenService } from '../../../pages/services/token/token.service';

@Component({
  selector: 'company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.scss']
})
export class CompanyListComponent implements OnInit {

  source: LocalDataSource; // add a property to the component

  settings = {
    columns: {
      companyName: {
        title: 'Company Name',
        filter: false
      },
      phone: {
        title: 'Phone',
        filter: false
      },
      mobile: {
        title: 'Mobile',
        filter: false
      },
      companyAddress: {
        title: 'Address',
        filter: false
      },
      noOfEmployees: {
        title: 'No. of Employees Assigned',
        filter: false
      },
      noOfSites: {
        title: 'No. of Sites',
        filter: false
      },
    },
    // add: {
    //   addButtonContent: '<i class="nb-plus"></i>',
    //   RemoveButtonContent: '<i class="nb-plus"></i>',
    //   editButtonContent: '<i class="far fa-eye"></i><i class="far fa-edit"></i>',
    //   deleteButtonContent: '<i class="far fa-eye"></i>',
    // },
    // edit: {
    //   editButtonContent: '<i class="far fa-eye"></i><i class="far fa-edit"></i>',
    // },
    // delete: {
    //   deleteButtonContent: '<i class="far fa-eye"></i>',
    // },

    actions: {
      position: 'right',
      edit: false,
      add: false,
      delete: false,

      // custom: [
      //   {
      //     name: 'employees',
      //     title: '<button class="button" type="button">Employees</button>'
      //   },
      //   {
      //     name: 'sites',
      //     title: '<button type="button">Sites</button>'
      //   },
      //   {
      //     name: 'scans',
      //     title: '<button type="button">Scans</button>'
      //   }
      // ],
    }
  };

  data = [
    // ... list of items        
  ];

  LoggedInAs;
  isLoading = true;
  companyLists: any = []
  companyListsLength = 0;
  providerName;
  employeeCount;
  siteCount;
  constructor(
    private CompanyServ: CompanyService,
    private TokenServ: TokenService,
    private ProviderServ: ProviderService,
    private router: Router,
    private EmployeeServ: EmployeeService
  ) {
    this.source = new LocalDataSource(this.data); // create the source   
    const result = this.TokenServ.getDecodedToken();
    const { role } = result;
    this.LoggedInAs = role;
  }

  // Get All Company Details

  async getAllCompanyDetails() {
    const getProviderId = this.TokenServ.getDecodedToken();
    const { providerId } = getProviderId;

    if (providerId) {
      this.getSelectedProviderDetails(providerId)
      const result: any = await this.CompanyServ.getAllproviderCompany(providerId);
      this.companyListsLength = result.length;
      this.companyLists = result;

      this.getEmployeeDetail();
    }

  }

  // Get Provider Details

  async getSelectedProviderDetails(id) {
    const result: any = await this.ProviderServ.getSelectedProvide(id);
    this.providerName = result.providerName;
  }

  async pageEvent(event) {
    this.isLoading = true;
    let start = event.pageIndex * event.pageSize;
    let end = event.pageSize;
    try {
      const result = await this.CompanyServ.getAllCompany(start, end);
      this.isLoading = false;
      this.companyLists = result;
      this.isLoading = false;
    }
    catch (error) {
      console.log(error)
    }
  }
  companyId: any;
  onUserRowSelect(event) {

    this.companyId = '';
    this.companyId = event.data.companyId;
    this.router.navigate(['./pages/company/details', this.companyId]);

  }

  onUserRowSelectEmployee(event) {

    this.companyId = '';
    this.companyId = event.companyId;
    this.router.navigate(['./pages/company/employees', this.companyId]);

  }

  onUserRowSelectSites(event) {

    this.companyId = '';
    this.companyId = event.companyId;
    this.router.navigate(['./pages/company/sites', this.companyId]);

  }

  onUserRowSelectScans(event) {

    this.companyId = '';
    this.companyId = event.companyId;
    this.router.navigate(['./pages/company/scans', this.companyId]);

  }

  onCustomAction(event) {
    switch (event.action) {
      case 'employees':
        this.onUserRowSelectEmployee(event.data);
        break;
      case 'sites':
        this.onUserRowSelectSites(event.data);
        break;
      case 'scans':
        this.onUserRowSelectScans(event.data);
        break;
    }
  }

  ngOnInit() {
    this.getAllCompanyDetails()
  }

  createcompany() {
    this.router.navigate(['./pages/company/details'])
  }

  comp: any;
  totalCount;
  presentCount;
  assigned: any = [];
  async getEmployeeDetail() {
    this.totalCount = 0;
    const results = await this.EmployeeServ.getAllEmployees();
    this.comp = results;
    if (this.LoggedInAs === 'super_admin') {
      this.companyLists.forEach(ele => {          // for loop for companies
        this.totalCount = 0;
        this.comp.forEach(ele1 => {          // for loop for employees
          ele1.companies.forEach(data => {          // for loop for companiesAssigned
            if (data.companyId === ele.companyId) {
              this.totalCount++;
            }
          })
        })
        Object.assign(ele, { noOfEmployees: this.totalCount })
      })
    }
    this.source.load(this.companyLists)
  }

}
