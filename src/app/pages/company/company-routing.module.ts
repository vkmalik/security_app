import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CompanyDetailsComponent } from './company-details/company-details.component';
import { CompanyListComponent } from './company-list/company-list.component';
import { EmployeesComponent } from './employees/employees.component';
import { ScansComponent } from './scans/scans.component';
import { SitesComponent } from './sites/sites.component';
import { CompanyComponent } from './company.component';
import { ProfileComponent } from './profile/profile.component';
import { LocationsComponent } from './locations/locations.component';
import { CreateEmployeeComponent } from './create-employee/create-employee.component';
import { CreateSitesComponent } from './create-sites/create-sites.component';
import { CompanyComponents } from './provider/company/company.component';
import { CompanyListComponents } from './provider/company-list/company-list.component';

const routes: Routes = [{
  path: '',
  component: CompanyComponent,
  children: [
    { path: 'list', component: CompanyListComponent },
    { path: 'details', component: CompanyDetailsComponent },
    { path: 'details/:id', component: CompanyDetailsComponent },
    { path: 'createEmployees', component: CreateEmployeeComponent },
    { path: 'createEmployees/:id', component: CreateEmployeeComponent },
    { path: 'employees', component: EmployeesComponent },
    { path: 'employees/:id', component: EmployeesComponent },
    { path: 'employees/:id/:siteId', component: EmployeesComponent },
    { path: 'scans', component: ScansComponent },
    { path: 'scans/:id', component: ScansComponent },
    { path: 'post', component: SitesComponent },
    { path: 'post/:id', component: SitesComponent },
    { path: 'createPosts', component: CreateSitesComponent },
    { path: 'createPosts/:id', component: CreateSitesComponent },
    { path: 'profile', component: ProfileComponent },
    { path: 'location', component: LocationsComponent },
    { path: 'location/:id', component: LocationsComponent },
    { path: 'provider/:id', component: CompanyComponents },
    { path: 'provider', component: CompanyComponents },
    { path: 'provider-list', component: CompanyListComponents },
  ],
}];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanyRoutingModule { }
