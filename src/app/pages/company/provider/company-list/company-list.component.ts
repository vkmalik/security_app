import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { LocalDataSource } from 'ng2-smart-table';

import { ProviderService } from '../../../services/provider/provider.service';
import { TokenService } from '../../../services/token/token.service';


@Component({
  selector: 'app-company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.scss']
})
export class CompanyListComponents implements OnInit {
  LoggedInAs = '';
  loginId = '';
  title = '';
  dashboardHeader = '';
  tableTitle = '';
  source: LocalDataSource; // add a property to the component

  settings = {
    columns: {
      providerName: {
        title: 'Name',
        filter: false
      },
      phone: {
        title: 'Phone',
        filter: false
      },
      mobile: {
        title: 'Mobile',
        filter: false
      },
      // noOfCompanys: {
      //   title: 'No. Of Companys',
      //   filter: false
      // },
      noOfEmployees: {
        title: 'No. Of Employees',
        filter: false
      },
      noOfGuards: {
        title: 'No. Of Guards',
        filter: false
      },
    },
    // add: {
    //   addButtonContent: '<i class="nb-plus"></i>',
    //   RemoveButtonContent: '<i class="nb-plus"></i>',
    //   editButtonContent: '<i class="far fa-eye"></i><i class="far fa-edit"></i>',
    //   deleteButtonContent: '<i class="far fa-eye"></i>',
    // },
    // edit: {
    //   editButtonContent: '<i class="far fa-eye"></i><i class="far fa-edit"></i>',
    // },
    // delete: {
    //   deleteButtonContent: '<i class="far fa-eye"></i>',
    // },

    actions: {
      position: 'right',
      edit: false,
      add: false,
      delete: false,

      // custom: [
      //   {
      //     name: 'employees',
      //     title: '<button class="button" type="button">Employees {{employeeCount}}</button>'
      //   },
      //   {
      //     name: 'sites',
      //     title: '<button type="button">Sites {{siteCount}}</button>'
      //   },
      //   {
      //     name: 'scans',
      //     title: '<button type="button">Scans</button>'
      //   }
      // ],
    }
  };

  data = [
    // ... list of items        
  ];

  isLoading = true;
  providerLists: any = []
  totalPageLength = 0
  startIndex = 0;

  constructor(
    private ProviderServ: ProviderService,
    private router: Router,
    private TokenServ: TokenService,
  ) {
    this.source = new LocalDataSource(this.data); // create the source
    this.getProviderList()
    const result = this.TokenServ.getDecodedToken();
    const { role, userId } = result;
    this.LoggedInAs = role;
    this.loginId = userId;
    if (role === 'owner') {
      this.title = 'Create New Provider'
      this.dashboardHeader = 'Providers List'
      // this.tableTitle = 'Provider'
    } else if (role === 'super_admin') {
      this.title = 'Create New Company'
      this.dashboardHeader = 'Company List'
      // this.tableTitle = 'Company'
    }
  }


  // Get All Provider List
  companyList: any = [];
  async getProviderList() {
    try {
      const getValue: any = await this.ProviderServ.getAllProvide();
      if (getValue) {
        getValue.forEach(ele => {
          if (this.LoggedInAs === 'owner') {
            if (ele.role === 'super_admin') {
              this.companyList.push(ele);
            }
          } else if (this.LoggedInAs === 'super_admin') {
            if (ele.role !== 'super_admin' && ele.role !== 'owner' && this.loginId === ele.createdBy) {
              this.companyList.push(ele);
            }
          }

        });
        this.source.load(this.companyList);
      }
    }
    catch (error) {
      console.log(error)
    }
  }



  ngOnInit() {
  }

  providerId;
  onUserRowSelect(event) {
    this.providerId = '';
    this.providerId = event.data.providerId;
    this.router.navigate(['./pages/company/provider', this.providerId]);
  }


  createcompany() {
    this.router.navigate(['./pages/company/provider'])
  }

}
