import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyListComponents } from './company-list.component';

describe('CompanyListComponent', () => {
  let component: CompanyListComponents;
  let fixture: ComponentFixture<CompanyListComponents>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyListComponents ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyListComponents);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
