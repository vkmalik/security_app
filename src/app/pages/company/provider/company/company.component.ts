import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertDialogService } from '../../../../alert-dialog/alert-dialog.service';
import { FormDataService } from '../../../services/lib/form-data.service';
import { ProviderService } from '../../../services/provider/provider.service';
import { SettingsService } from '../../../services/settings/settings.service';
import { TokenService } from '../../../services/token/token.service';
import { AuthService } from '../../../services/user/auth.service';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponents implements OnInit {
  loggedInAs = '';
  loginId = '';
  title = '';
  dashboardHeader = '';
  date;
  user: any = {}
  edit = false;
  userSelectedFile;
  imageUrlPath;
  constructor(
    private ProviderServ: ProviderService,
    private router: Router,
    private route: ActivatedRoute,
    private FormDataServ: FormDataService,
    private SettingsSerc: SettingsService,
    private TokenServ: TokenService,
    private AuthServ: AuthService,
    private AlertDialog: AlertDialogService
  ) {
    this.date = new Date();
    this.getProviderList();
    const result = this.TokenServ.getDecodedToken();
    const { role, userId } = result;
    this.loggedInAs = role;
    this.loginId = userId;
    if (role === 'owner') {
      this.title = 'Provider'
      this.dashboardHeader = 'Provider Details'
    } else if (role === 'super_admin') {
      this.title = 'Company'
      this.dashboardHeader = 'Company Details'
    }
  }

  // Create New Provider
  async createNewProvider(formData) {

    if (this.edit !== true) {
      this.providerList.forEach(async (ele) => {
        if (ele.providerName === formData.value.providerName) {
          return await this.AlertDialog.confirm('Alert Message', 'Company name already exists. Please enter a different name.');
        }
      })

      if (formData.value.providerName === undefined ||
        formData.value.email === undefined ||
        formData.value.password === undefined ||
        formData.value.providerAddress === undefined ||
        // formData.value.noOfCompanys === undefined ||
        formData.value.noOfEmployees === undefined ||
        formData.value.noOfGuards === undefined ||
        formData.value.licenseStartDate === undefined ||
        formData.value.licenseTrailEndDate === undefined ||
        formData.value.licenseEndDate === undefined) {
        return await this.AlertDialog.confirm('Alert Message', 'Please Fill the Mandatory Fields');
      }
    }

    const { value } = formData;

    try {
      if (this.edit === true) {
        const fdData = this.FormDataServ.changeFormdataServ('providerLogo', this.userSelectedFile, this.user)
        let result = await this.ProviderServ.updateSelectedProvide(this.user.providerId, fdData)
        return this.router.navigate(['./pages/company/provider-list'])
      }
      if (this.loggedInAs === 'owner') {
        Object.assign(value, { role: 'super_admin', createdBy: this.loginId })
      } else if (this.loggedInAs === 'super_admin') {
        Object.assign(value, { role: 'Admin', createdBy: this.loginId })
      }
      const fdData = this.FormDataServ.changeFormdataServ('providerLogo', this.userSelectedFile, value)
      let result = await this.ProviderServ.createProvide(fdData)
      if (result) {
        return this.router.navigate(['./pages/company/provider-list'])
      }
    }
    catch (error) {
      console.log(error)
    }
  }


  // Get Prarticular Provider Details

  async getProviderDetail(id) {
    try {
      const result: any = await this.ProviderServ.getSelectedProvide(id)
      result.profilePic = `${this.SettingsSerc.IMAGE_URL}${result.profilePic}`
      this.user = result;
      this.edit = true;
    }
    catch (error) {
      console.log(error)
    }
  }

  // Select Company Logo Process

  readURL(files) {
    const [file] = files;
    this.userSelectedFile = file;
  }


  ngOnInit() {
    let getID = this.route.snapshot.paramMap.get("id");
    if (getID === '' || getID === undefined || getID === null) {
      return this.user = {}
    }
    this.getProviderDetail(getID)
  }

  close() {
    this.router.navigate(['./pages/company/provider-list'])
  }

  value: any = {}
  async checkEmail(event) {
    try {
      Object.assign(this.value, { 'email': event })
      const result: any = await this.AuthServ.getUserEmail(this.value);
      if (result.length === 0) {
        return;
      } else {
        await this.AlertDialog.confirm('Alert Message', 'This Email already exists');
      }
    }
    catch (error) {
      console.log(error);
    }
  }

  providerList: any = [];
  async getProviderList() {
    try {
      const getValue: any = await this.ProviderServ.getAllProvide();
      this.providerList = getValue;
    }
    catch (error) {
      console.log(error)
    }
  }

  employeeCount;
  getNoOfEmployees(event) {
    this.employeeCount = event;
  }

  more = false;
  guardCount;
  async getNoOfGuards(event) {
    this.guardCount = event;

    if (this.employeeCount < this.guardCount) {
      this.more = true;
      const result = await this.AlertDialog.confirm('Alert Message', 'No of Guards can not be more than no of employees.');
      return;
    } else {
      this.more = false;
    }
  }
}
