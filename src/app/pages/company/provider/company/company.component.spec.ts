import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyComponents } from './company.component';

describe('CompanyComponents', () => {
  let component: CompanyComponents;
  let fixture: ComponentFixture<CompanyComponents>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyComponents ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyComponents);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
