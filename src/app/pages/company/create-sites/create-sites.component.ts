import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertDialogService } from '../../../alert-dialog/alert-dialog.service';
import { CompanyService } from '../../services/company/company.service';
import { ProviderService } from '../../services/provider/provider.service';
import { SiteService } from '../../services/site/site.service';
import { TokenService } from '../../services/token/token.service';


@Component({
  selector: 'app-create-site',
  templateUrl: './create-sites.component.html',
  styleUrls: ['./create-sites.component.scss']
})
export class CreateSitesComponent implements OnInit {
  loginId;
  user: any = {};
  id;
  edit = false;
  companyId = '';
  seasons = ['Yes', 'No']
  constructor(
    private SiteServ: SiteService,
    private CompanyServ: CompanyService,
    private router: Router,
    private ProviderServ: ProviderService,
    private route: ActivatedRoute,
    private TokenServ: TokenService,
    private AlertDialog: AlertDialogService
  ) {
    // this.getProviderList()
    this.getCompanyList()
    const result = this.TokenServ.getDecodedToken();
    const { userId } = result;
    this.loginId = userId;
  }

  ngOnInit() {
    let getID = this.route.snapshot.paramMap.get("id");
    if (getID === '' || getID === undefined || getID === null) {
      return;
    }
    // this.companyId = getID;
    this.getSiteDetail(getID)
  }

  // Get All Provider List
  companyList: any = [];
  // async getProviderList() {
  //   try {
  //     const getValue: any = await this.ProviderServ.getAllProvide();
  //     if (getValue) {
  //       getValue.forEach(ele => {
  //         if (ele.role !== 'super_admin' && ele.role !== 'owner' && this.loginId === ele.createdBy) {
  //           this.companyList.push(ele);
  //         }
  //       });
  //     }
  //   }
  //   catch (error) {
  //     console.log(error)
  //   }
  // }

  companyArray;
  async getCompanyList() {
    const getProviderId = this.TokenServ.getDecodedToken();
    const { providerId } = getProviderId;
    const result: any = await this.CompanyServ.getAllproviderCompany(providerId);
    this.companyArray = result;
    this.companyArray.forEach(ele => {
      this.companyList.push(ele);
    })
  }

  providerName(event) {
    this.companyId = event;
  }

  // Get Prarticular Site Details

  async getSiteDetail(id) {
    try {
      const result: any = await this.SiteServ.getSelectedSite(id);
      if (result.message === "No Record Found this ID") {
        this.edit = false;
        return
      }
      this.user = result;
      this.edit = true;
      this.id = result.companyId;
    }
    catch (error) {
      console.log(error)
    }
  }

  async createNewsite(formData) {
    if (formData.value.siteName === undefined || formData.value.siteNoOfQrcodes === undefined || formData.value.Address === undefined) {
      await this.AlertDialog.confirm('Alert Message', 'Please Fill the Mandatory Fields');
      return;
    }
    if (this.companyId === '' && this.edit === false) {
      await this.AlertDialog.confirm('Alert Message', 'Please Choose Company Name.');
      return;
    }
    const { value } = formData;
    const result = this.TokenServ.getDecodedToken();
    const { providerId, userId, companyId, name } = result;
    Object.assign(value, { providerId, userId, companyId: this.companyId, createdBy: name })
    if (!this.companyId) {
      console.log(companyId)
      Object.assign(value, { providerId, userId, companyId, createdBy: name })
    }
    try {
      if (this.edit === true) {
        let result: any = await this.SiteServ.updateSelectedsite(this.user.siteId, this.user)
        this.id = '';
        this.id = result.companyId;
        return this.router.navigate([`./pages/company/post/${result.companyId}`])

      }
      const createResult: any = await this.SiteServ.createSite(value)
      return this.router.navigate([`./pages/company/post/${this.companyId}`])
    }
    catch (error) {
      console.log(error)
    }
  }

  close() {

    if (this.edit !== true) {
      this.router.navigate([`./pages/company/post/${this.companyId}`])
    } else {
      this.router.navigate([`./pages/company/post/${this.id}`])
    }

  }

}
