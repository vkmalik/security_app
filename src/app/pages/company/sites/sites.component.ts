import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { LocalDataSource } from 'ng2-smart-table';

import { CompanyService } from '../../services/company/company.service';
import { ProviderService } from '../../services/provider/provider.service';
import { SiteService } from '../../services/site/site.service';
import { TokenService } from '../../services/token/token.service';

@Component({
  selector: 'sites',
  templateUrl: './sites.component.html',
  styleUrls: ['./sites.component.scss']
})
export class SitesComponent implements OnInit {
  loginId;
  seasons = ['Yes', 'No'];
  showDetails = false;
  empty = false;
  source: LocalDataSource; // add a property to the component

  settings = {
    columns: {
      // slNo: {
      //   title: '#',
      //   filter: false
      // },
      siteName: {
        title: 'Post Name',
        filter: false
      },
      Address: {
        title: 'Address',
        filter: false
      },
      scanIntervals: {
        title: 'Scan interval',
        filter: false
      },
      siteNoOfQrcodes: {
        title: 'No. of Locations',
        filter: false
      },
      createdBy: {
        title: 'Created By',
        filter: false
      },
    },
    // add: {
    //   addButtonContent: '<i class="nb-plus"></i>',
    //   editButtonContent: '<i class="far fa-eye"></i><i class="far fa-edit"></i>',
    //   deleteButtonContent: '<i class="far fa-eye"></i>',
    // },
    // edit: {
    //   editButtonContent: '<i class="far fa-eye"></i><i class="far fa-edit"></i>',
    // },
    // delete: {
    //   deleteButtonContent: '<i class="far fa-eye"></i>',
    // },

    actions: {
      position: 'right',
      edit: false,
      add: false,
      delete: false,
      custom: [
        {
          name: 'locations',
          title: '<button class="button" type="button">View Locations</button>'
        }
      ],
    }
  };

  data = [
    // ... list of items        
  ];


  user: any = {};
  edit = false;
  siteLists: any = [];
  isAdmin = true;
  siteId: any = '';
  companyId;
  CompanyName;
  role
  constructor(
    private route: ActivatedRoute,
    private SiteServ: SiteService,
    private TokenServ: TokenService,
    private CompanyServ: CompanyService,
    private router: Router,
    private ProviderServ: ProviderService,
  ) {
    const result = this.TokenServ.getDecodedToken();
    if (!result) {
      this.TokenServ.removeToken();
      return;
    }
    this.source = new LocalDataSource(this.data); // create the source
    // this.getProviderList()
    this.getCompanyList();
  }

  routeId;
  ngOnInit() {
    let getID = this.route.snapshot.paramMap.get("id");
    this.routeId = getID;
    const getProviderId = this.TokenServ.getDecodedToken();
    const { companyId, siteId, role, userId } = getProviderId;
    this.role = role;
    this.companyId = getID;
    this.loginId = userId;
    if (!getID) {
      this.companyId = companyId;
    }
    if (!siteId && siteId !== '') {
      this.getCompanyDetail(getID);
      return
    }
    this.getCompanyDetail(companyId);
    this.siteId = siteId;
    this.isAdmin = false;
  }

  // Get All Provider List
  companyList: any = [];
  companyArray;
  async getCompanyList() {
    const getProviderId = this.TokenServ.getDecodedToken();
    const { providerId } = getProviderId;
    const result: any = await this.CompanyServ.getAllproviderCompany(providerId);
    this.companyArray = result;
    this.companyArray.forEach(ele => {
      this.companyList.push(ele);
    })

    if (this.routeId === null && this.companyList.length !== 0) {
      this.providerName(this.companyList[0].companyId);
    } else {
      this.providerName(this.routeId);
    }

    if (this.companyList.length === 0) {
      this.empty = true;
    } else {
      this.empty = false;
    }
  }

  siteNameList: any;
  loginDetailsList: any;
  show = false;
  async providerName(event) {               // event is companyId
    const result: any = await this.SiteServ.getAllSite(event);
    this.siteNameList = '';
    this.siteNameList = result;
    this.source.load(this.siteNameList);
  }

  async getCompanyDetail(id) {
    const result: any = await this.CompanyServ.getSelectedCompany(id);
    this.CompanyName = result.companyName;
  }

  addNewSite() {
    this.router.navigate([`./pages/company/createPosts`]);

  }

  onUserRowSelect(event) {

    this.siteId = '';
    this.siteId = event.data.siteId;
    this.router.navigate([`./pages/company/createPosts`, this.siteId]);

  }

  locId;
  onUserRowSelectLocation(event) {

    this.locId = '';
    this.locId = event.siteId;
    this.router.navigate(['./pages/company/location', this.locId]);

  }

  onCustomAction(event) {
    this.onUserRowSelectLocation(event.data);
  }

  back() {
    this.router.navigate(['./pages/company/list'])
  }

}
