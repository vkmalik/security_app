import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';

import { LocalDataSource } from 'ng2-smart-table';

import { AlertDialogService } from '../../../alert-dialog/alert-dialog.service';
import { LocationService } from '../../services/location/location.service';
import { SiteService } from '../../services/site/site.service';
import { TokenService } from '../../services/token/token.service';

@Component({
  selector: 'locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.scss']
})
export class LocationsComponent implements OnInit {

  locationLists: any = [];
  showDetails = false;
  siteId = '';
  companyId;
  siteName;
  locationName;
  qrCodes: any;
  full = false;
  public myAngularxQrCode = null;
  source: LocalDataSource; // add a property to the component

  settings = {
    columns: {
      // slNo: {
      //   title: '#',
      //   filter: false
      // },
      locationName: {
        title: 'Location Name',
        filter: false
      },
      latitude: {
        title: 'Latitude',
        filter: false
      },
      longitude: {
        title: 'Longitude',
        filter: false
      },
      locDifference: {
        title: 'Location Difference (in meters)',
        filter: false
      },
      createdAt: {
        title: 'Created Date',
        filter: false,
        valuePrepareFunction: (date) => {
          var raw = new Date(date);

          var formatted = this.datePipe.transform(raw, 'dd-MM-yyyy');
          return formatted;
        }
      },
      createdBy: {
        title: 'Created By',
        filter: false
      },
    },
    // add: {
    //   addButtonContent: '<i class="nb-plus"></i>',
    //   editButtonContent: '<i class="far fa-eye"></i><i class="far fa-edit"></i>',
    //   deleteButtonContent: '<i class="far fa-eye"></i>',
    // },
    // edit: {
    //   editButtonContent: '<i class="far fa-eye"></i><i class="far fa-edit"></i>',
    // },
    // delete: {
    //   deleteButtonContent: '<i class="far fa-eye"></i>',
    // },

    actions: {
      position: 'right',
      edit: false,
      add: false,
      delete: false,
      // custom: [
      //   {
      //     name: 'view',
      //     title: '<i class="far fa-eye"></i>',
      //   },
      // ],
    }
  };

  data = [
    // ... list of items        
  ];

  constructor(
    private LocationServ: LocationService,
    private route: ActivatedRoute,
    private TokenServ: TokenService,
    private SiteServ: SiteService,
    private datePipe: DatePipe,
    private router: Router,
    private AlertDialog: AlertDialogService
  ) {
    this.source = new LocalDataSource(this.data); // create the source
  }

  async createNewLocation(formData) {
    if (formData.value.locationName === '' || formData.value.latitude === '' || formData.value.longitude === '' || formData.value.locDifference === '') {

      this.locationLists.forEach(async (ele) => {
        if (ele.locationName === formData.value.locationName) {
          return await this.AlertDialog.confirm('Alert Message', 'Location with same name already exists.');
        }
      })
      return await this.AlertDialog.confirm('Alert Message', 'Please Fill the Mandatory Fields');

    }



    const { value } = formData;
    const result = this.TokenServ.getDecodedToken();
    const { providerId, userId, name } = result;
    Object.assign(value, { providerId, userId, createdBy: name, siteId: this.siteId })
    await this.LocationServ.createLocation(value);
    this.showDetails = !this.showDetails
    this.getLocationDetail(this.siteId)
  }

  viewQrcode(data) {
    this.locationName = data.locationName;
    this.myAngularxQrCode = JSON.stringify(data);
  }


  openTextBox() {
    this.showDetails = !this.showDetails
  }

  // Get Site detail
  async getSiteDetail(id) {
    const result: any = await this.SiteServ.getSelectedSite(id);
    this.qrCodes = result.siteNoOfQrcodes;
    this.siteName = result.siteName;
    this.companyId = result.companyId;
    this.siteId = result.siteId;

    this.getLocationDetail(id)
  }

  // Get Location detail
  async getLocationDetail(id) {
    const result = await this.LocationServ.getAllLocation(id);
    this.locationLists = result;
    this.source.load(this.locationLists);

    // if (parseInt(this.qrCodes) === parseInt(this.locationLists.length)) {
    //   this.full = true;
    // } else {
    //   this.full = false;
    // }
  }


  ngOnInit() {
    let getID = this.route.snapshot.paramMap.get("id");
    const result = this.TokenServ.getDecodedToken();
    if (getID === '' || getID === undefined || getID === null) {
      this.siteId = '';
      return
    }
    this.siteId = getID;
    // this.getLocationDetail(getID)
    this.getSiteDetail(getID)
  }

  addNewLocation() {
    this.showDetails = true;
  }

  close() {
    this.showDetails = false;
  }

  cancel() {
    this.router.navigate(['./pages/company/post', this.companyId])
  }

}
