import { Component } from '@angular/core';

import { MENU_ITEMS, MENU_ITEMS1 } from './pages-menu';

import { TokenService } from './services/token/token.service';

@Component({
  selector: 'ngx-pages',
  template: `
    <ngx-sample-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-sample-layout>
  `,
})
export class PagesComponent {
  menu;
  constructor(
    private TokenServ: TokenService,
  ) {
    this.getrole()
  }

  getrole() {
    const result = this.TokenServ.getDecodedToken();
    const { role } = result;
    // console.log(role)
    if(role === 'super_admin'){
     this.menu = MENU_ITEMS1; 
    }else if(role === 'owner'){
      this.menu = MENU_ITEMS;
    }
  }

  
}
