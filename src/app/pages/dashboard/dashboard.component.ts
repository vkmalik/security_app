import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';

import { async } from 'q';

import { scan } from 'rxjs/operators';

import { CompanyService } from '../services/company/company.service';
import { EmployeeService } from '../services/employee/employee.service';
import { ProviderService } from '../services/provider/provider.service';
import { ScanService } from '../services/scan-details/scan.service';
import { SiteService } from '../services/site/site.service';
import { TokenService } from '../services/token/token.service';
import { AuthService } from '../services/user/auth.service';

@Component({
  selector: 'ngx-dashboard',
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent {
  companyCount = 0;
  employeeCount = 0;
  LoggedInAs = '';
  loginId = '';
  title = '';
  dashboardHeader = '';
  owner = false;
  empty = false;
  show = false;
  count = 0;
  countPresent = 0;
  companyListsLength: any;
  date;
  formatted;
  companyLists: any;
  constructor(
    private CompanyServ: CompanyService,
    private TokenServ: TokenService,
    private ProviderServ: ProviderService,
    private router: Router,
    private SiteServ: SiteService,
    private EmployeeServ: EmployeeService,
    private ScanSer: ScanService,
    private authServ: AuthService,
    private datePipe: DatePipe,
  ) {


    this.date = new Date();
    this.formatted = this.datePipe.transform(this.date, 'dd/MM/yyyy');
    const result = this.TokenServ.getDecodedToken();
    const { role, userId } = result;
    this.LoggedInAs = role;
    this.loginId = userId;
    if (role === 'owner') {
      this.getProviderList()
      this.owner = true;
      this.title = 'No Providers Added. Please add.'
      this.dashboardHeader = 'Providers'
    } else if (role === 'super_admin') {
      this.getCompanyList()
      this.owner = false;
      this.title = 'No Companies Added. Please add.'
      this.dashboardHeader = 'Site Attendance'
    }
  }
  // Get All Provider List
  companyList: any = [];
  totalCount = 0;
  presentCount = 0;
  assigned: any = [];
  companies: any = [];
  async getProviderList() {
    try {
      const getValue: any = await this.ProviderServ.getAllProvide();
      if (getValue) {
        getValue.forEach(ele => {
          if (this.LoggedInAs === 'owner') {
            if (ele.role === 'super_admin') {
              this.companyList.push(ele);
            }
          }
        });

        if (this.companyList.length === 0) {
          this.empty = true;
        } else {
          this.empty = false;
          if (this.LoggedInAs === 'owner') {
            this.companyList.forEach(async (ele, i) => {
              this.companyListsLength = 0;
              const result: any = await this.CompanyServ.getAllproviderCompany(ele.providerId);
              this.companyListsLength = result.length;
              this.companies.push({ 'companyCount': this.companyListsLength })
            })
          }
        }
      }
      this.getEmployeeDetail();
    }
    catch (error) {
      console.log(error)
    }
  }

  async getCompanyList() {
    const getProviderId = this.TokenServ.getDecodedToken();
    const { providerId } = getProviderId;

    if (providerId) {
      const result: any = await this.CompanyServ.getAllproviderCompany(providerId);
      this.companyListsLength = result.length;
      this.companyList = result;

      if (this.companyList.length === 0) {
        this.empty = true;
      } else {
        this.empty = false;
        this.companyList.forEach((ele, i) => {
          this.totalCount = 0;
          this.presentCount = 0;
          this.assigned.push({ 'companyId': ele.companyId, 'total': this.totalCount, 'present': this.presentCount, 'employees': [] });
          this.companyName(this.companyList[i].companyId);
        })
      }
    }

    this.getEmployeeDetail();
  }

  scans: any;
  users: any;
  scannedCount = 0;
  async scanDetails() {
    const result = await this.ScanSer.getScanRecord();
    this.scans = result;
    this.assigned.forEach(data => {
      data.employees.forEach(ele1 => {
        this.scans.forEach(ele => {
          if (ele.scannedDate === this.formatted) {
            if (ele1.employeeId === ele.employeeId) {
              this.scannedCount++;
              if (this.scannedCount === 1) {
                this.presentCount++;
                Object.assign(data, { 'present': this.presentCount })
              }
            }
          }
        })
        this.scannedCount = 0;
      })
    })
  }

  comp: any;
  employees: any = [];
  async getEmployeeDetail() {
    this.totalCount = 0;
    const results = await this.EmployeeServ.getAllEmployees();
    this.comp = results;
    if (this.LoggedInAs === 'super_admin') {
      this.comp.forEach(ele => {
        ele.companies.forEach(ele1 => {
          this.assigned.forEach((data, i) => {
            if (ele1.companyId === data.companyId) {
              if (data.total === 0) {
                this.totalCount++;
                this.employees.push({ 'employeeId': ele.employeeId })
                Object.assign(data, { 'total': this.totalCount, 'employees': this.employees })
                this.totalCount = 0;
              } else {
                this.totalCount = data.total + 1;
                this.employees.push({ 'employeeId': ele.employeeId })
                Object.assign(data, { 'total': this.totalCount, 'employees': this.employees })
                this.totalCount = 0;
              }
            } else {
            }
          })
        })
        this.totalCount = 0;
      })
    } else {
      this.companyList.forEach((ele, i) => {
        this.employeeCount = 0;
        this.comp.forEach((ele1, j) => {
          if (ele.providerId === ele1.providerId) {
            this.employeeCount++;
          }
          if (j === this.comp.length - 1) {
            Object.assign(this.companies[i], { 'employeeCount': this.employeeCount })
          }
        })
      })
    }
    this.scanDetails();
  }

  getEmployees(event) {               // event is companyId
    this.router.navigate(['./pages/company/employees', event])
  }

  getScans(event) {               // event is companyId
    this.router.navigate(['./pages/company/scans', event])
  }

  getPosts(event) {               // event is companyId
    this.router.navigate(['./pages/company/post', event])
  }

  getCompanyDetails(event) {               // event is companyId
    this.router.navigate(['./pages/company/details', event])
  }

  siteNameList;
  siteArray = [];
  async companyName(event) {
    const result: any = await this.SiteServ.getAllSite(event);
    this.siteNameList = '';
    this.siteNameList = result;
    this.siteArray.push(this.siteNameList)
    // console.log(this.siteNameList)
  }
}
