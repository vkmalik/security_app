import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'nb-home',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'FEATURES',
    group: true,
  },
  {
    title: 'Providers',
    icon: 'nb-layout-default',
    children: [
      {
        title: 'Provider List',
        link: '/pages/company/provider-list',
      },
      // {
      //   title: 'Employees',
      //   link: '/pages/company/employees',
      // },
      // {
      //   title: 'Post',
      //   link: '/pages/company/post',
      // },
      // {
      //   title: 'Scans',
      //   link: '/pages/company/scans',
      // },
    ],
  },
];

export const MENU_ITEMS1: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'nb-home',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'FEATURES',
    group: true,
  },
  {
    title: 'Company',
    icon: 'nb-layout-default',
    children: [
      {
        title: 'Company List',
        link: '/pages/company/list',
      },
      {
        title: 'Employees',
        link: '/pages/company/employees',
      },
      {
        title: 'Post',
        link: '/pages/company/post',
      },
      {
        title: 'Scans',
        link: '/pages/company/scans',
      },
    ],
  },
];
