import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AlertDialogService } from '../../alert-dialog/alert-dialog.service';
import { ProviderService } from '../../pages/services/provider/provider.service';
import { TokenService } from '../../pages/services/token/token.service';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(
    private ProviderServ: ProviderService,
    private router: Router,
    private AlertDialog: AlertDialogService
  ) { }

  ngOnInit() {
  }

  async registerForm(formData) {
    const { value } = formData;
    try {
      const result: any = await this.ProviderServ.userRegister(value)
      console.log(result)
      if(result.message === 'User Already Exist'){
        await this.AlertDialog.confirm('Alert Message', 'User with same Email Id already exists. Please Login.');
        this.router.navigate(['/auth/login']);
      }else{
        await this.AlertDialog.confirm('Alert Message', 'User Registered Successfully');
        this.router.navigate(['/auth/login']);
      }
      
    }
    catch (error) {      
      console.log(error)
    }

  }

}
