import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AlertDialogService } from '../../alert-dialog/alert-dialog.service';
import { TokenService } from '../../pages/services/token/token.service';
import { AuthService } from '../../pages/services/user/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    private AuthServ: AuthService,
    private TokenServ: TokenService,
    private router: Router,
    private AlertDialog: AlertDialogService
  ) { }

  async ngOnInit() {
    console.log(localStorage.token)
    if (localStorage.token !== undefined) {
      this.TokenServ.token = localStorage.token;
      const result = this.TokenServ.getDecodedToken();
      const { email } = result;
      const data: any = {};
      Object.assign(data, { 'email': email })
      const result1: any = await this.AuthServ.userLogoutEmail(data)
      localStorage.removeItem('token')
    }
  }

  async loginForm(formData) {
    const { value } = formData;
    try {
      const result: any = await this.AuthServ.userLogin(value)
      if (result.success === false) {
        await this.AlertDialog.confirm('Alert Message', 'User Already Logged-In.');
      } else {
        this.TokenServ.token = result.token;

        this.router.navigate(['/pages/dashboard']);
        this.TokenServ.userSubject.next(true);
        this.getrole();
      }
    }
    catch (error) {
      await this.AlertDialog.confirm('Alert Message', 'Given Email ID or Password is Wrong');
      console.log(error)
    }

  }

  async getrole() {
    const result = this.TokenServ.getDecodedToken();
    const { role } = result;
    if (role === 'owner') {
      this.router.navigate(['/pages/dashboard']);
    } else if (role === 'super_admin') {
      this.router.navigate(['/pages/dashboard']);
    } else if (role === 'admin') {
      this.router.navigate(['/pages/dashboard']);
    } else if (role === 'Employee') {
      await this.AlertDialog.confirm('Alert Message', 'Sorry You Are Not Authorized');
      this.router.navigate(['']);
    } else {

    }
  }

}
